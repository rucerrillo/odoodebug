var activeurl = "about:blank";

var newUrl = "";

browser.browserAction.onClicked.addListener(() => {
  
  function getActiveTab() {
		return browser.tabs.query({currentWindow: true, active: true});
	}

  getActiveTab().then(function (data) {
		  console.log(data[0].url);
		  newUrl = data[0].url;
		  
	});
	urlArray = newUrl.split("#");
	if (urlArray.length >= 2 && !newUrl.includes("debug")){

		newUrl = urlArray[0] + '?debug#' + urlArray[1];
		browser.tabs.update({url: newUrl});
		browser.browserAction.setIcon({path:'../button/odebug-o-32.png'})

	}else if (newUrl.includes("debug")) {

		urlArray = newUrl.split("?debug");
		newUrl = urlArray[0] + urlArray[1];
		browser.tabs.update({url: newUrl});
		browser.browserAction.setIcon({path:'../button/odebug-48.png'})

	}
});
